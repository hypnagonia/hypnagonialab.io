#!/usr/bin/env node

const path = require('path')
const webpack = require('webpack')
const webpackConfig = require('./webpackConfig.js')
const WebpackDevServer = require('webpack-dev-server')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const config = require('../../config')

const isDevMode = !process.argv.find(a => a === 'prod')
run(isDevMode)

function run (isDevMode = false) {

  if (isDevMode) {
    config.host.apiUrl = '/api'
  } else {
    config.host.apiUrl = `${(config.host.ssl ? 'https' : 'http')}://${config.host.name}`
  }

  webpackConfig.plugins.push(new webpack.DefinePlugin({
    'global.appConfig': JSON.stringify(config),
    'process.env': {
      NODE_ENV: isDevMode
        ? JSON.stringify('dev')
        : JSON.stringify('production')
    }
  }))

  webpackConfig.plugins.push(
    new webpack.optimize.CommonsChunkPlugin({
      name: ['common']
    })
  )

  //set entry point
  webpackConfig.entry = {
    main: [path.resolve(`src/app/index.js`)],
    common: [
      'prop-types',
      'react',
      'react-dom',
      'antd'
    ]
  }

  if (isDevMode) {
    const port = 8080

    webpackConfig.plugins.push(
      new webpack.HotModuleReplacementPlugin({
        // Options...
      })
    )

    webpackConfig.entry.main.unshift(
      `webpack-dev-server/client?http://localhost:${port}/`,
      'webpack/hot/dev-server'
    )

    const compiler = webpack(webpackConfig)

    const server = new WebpackDevServer(compiler, {
        hot: true,
        historyApiFallback: true,
        stats: {
          colors: true,
          chunks: false
        },
        proxy: {
          [`${config.host.apiUrl}`]: {
            target: {
              host: `${config.host.name}`,
              protocol: config.host.ssl ? 'https:' : 'http:',
              port: 3001
            },
            pathRewrite: {[config.host.apiUrl]: ''},
            changeOrigin: true,
            secure: false
          }
        }
      }
    )
    server.listen(port)
  }
  else {
    const compiler = webpack(webpackConfig)

    compiler.run((err, stats) => {
    })
  }
}

module.exports = run
