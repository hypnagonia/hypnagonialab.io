import React from 'react'
import { Table, Button } from 'antd'
import { displayDateFromNumbers } from '../../../utils'

const columns = [
  {
    title: '#',
    dataIndex: 'id'
  },
  {
    title: 'ФИО',
    dataIndex: 'name'
  },
  {
    title: 'Дата рождения',
    dataIndex: 'birthDate',
    render: birthDate => displayDateFromNumbers(birthDate)
  },
  {
    title: 'Адрес',
    dataIndex: 'address'
  },
  {
    title: 'Город',
    dataIndex: 'city'
  },
  {
    title: 'Телефон',
    dataIndex: 'phone'
  }
]

export class UsersList extends React.Component {
  constructor () {
    super()
    this.state = {
      users: [],
      isUserFormModalVisible: false,
      selectedUser: {}
    }
  }

  componentWillMount () {
  }

  render () {
    const {users} = this.state

    const controls = {
      fixed: 'right',
      width: 70,
      render: (_, user) => {
        return (
          <div className='buttons' style={{display: 'flex', justifyContent: 'space-between', width: 70}}>
            <Button
              type='primary'
              shape='circle'
              icon='edit'
              title='Редактировать' />
          </div>
        )
      }
    }

    return (
      <div>
        <Table
          scroll={{x: true}}
          rowKey='id'
          columns={[...columns, controls]}
          dataSource={users}
          pagination={false}
        />
      </div>
    )
  }
}
